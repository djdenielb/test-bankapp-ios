//
//  FormularioViewController.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import UIKit

class FormularioViewController: UIViewController {

//    Variables graficas
    @IBOutlet weak var tvNumeroTarjeta: UITextField!
    @IBOutlet weak var labelCuenta: UITextField!
    @IBOutlet weak var LabelIssue: UITextField!
    @IBOutlet weak var labelNombreTarjeta: UITextField!
    @IBOutlet weak var labelMarca: UITextField!
    @IBOutlet weak var labelEstatus: UITextField!
    @IBOutlet weak var labelSaldo: UITextField!
    @IBOutlet weak var labelTipoCuenta: UITextField!
    
//    Funcion al arrancar
    override func viewDidLoad() {
        super.viewDidLoad()
    }
//    Boton agregar, este mostrara una alerta con la informacion que ingrese el usuario en estilo de formato json
    @IBAction func btnAgregar(_ sender: Any) {
        
//        Variable que contiene los textview y tiene el estilo json
        let formularioFormatoJson = """
 "formulario":{
  "numeroTarjeta": \(tvNumeroTarjeta.text ?? "")
  "cuenta": \(labelCuenta.text ?? "")
  "issue": \(LabelIssue.text ?? "")
  "nombeTarjeta": \(labelNombreTarjeta.text ?? "")
  "marca": \(labelMarca.text ?? "")
  "estatus": \(labelEstatus.text ?? "")
  "tipo": \(labelTipoCuenta.text ?? "")
   }
 """
        
//        Creacion de la alerta
        let alerta = UIAlertController(title: "FormatoJson", message: formularioFormatoJson, preferredStyle: .alert)
        let botonAlerta = UIAlertAction(title: "ok", style: .default, handler: nil)
        alerta.addAction(botonAlerta)
        present(alerta, animated: true, completion: nil)
    }
    
//    Boton cancelar solo cierra la ventana de formulario
    @IBAction func btnCancelar(_ sender: Any) {
        dismiss(animated: true)
    }
    
//    Funcion para cerrar teclado al tocar fuera en cualquier lugar
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                self.view.endEditing(true)
            }
    
}
