//
//  ViewController.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 24/05/22.
//

import UIKit

class ViewController: UIViewController {
    
//    Declaramos variables para instanciar los managers y los arrays del tipo de los data
    var arrayCuenta: [Cuenta]?
    var arraySaldos: [Saldos]?
    var arrayTarjetas: [Tarjetas]?
    var arrayMov: [Movimiento]?
    var managerCuenta = ManagerCuenta()
    var managerSaldos = ManagerSaldos()
    var managerTarjetas = ManagerTarjetas()
    var managerMov = ManagerMov()
    
//    Variables graficas
    @IBOutlet weak var labelNombreUsuario: UILabel!
    @IBOutlet weak var labelUltimoInicio: UILabel!
    @IBOutlet weak var labelSaldoGeneral: UILabel!
    @IBOutlet weak var labelTotalIngresos: UILabel!
    @IBOutlet weak var ivTarjeta1: UIImageView!
    @IBOutlet weak var ivTarjeta2: UIImageView!
    @IBOutlet weak var labelEstado1: UILabel!
    @IBOutlet weak var labelNumTarjeta1: UILabel!
    @IBOutlet weak var labelNombre1: UILabel!
    @IBOutlet weak var labelTipo1: UILabel!
    @IBOutlet weak var labelSaldo1: UILabel!
    @IBOutlet weak var labelEstado2: UILabel!
    @IBOutlet weak var labelNumTarjeta2: UILabel!
    @IBOutlet weak var labelNombre2: UILabel!
    @IBOutlet weak var labelTipo2: UILabel!
    @IBOutlet weak var labelSaldo2: UILabel!
    
//    Variable grafica de la tabla
    @IBOutlet weak var tablaMov: UITableView!
    
//    Funcion al arrancar
    override func viewDidLoad() {
        super.viewDidLoad()
        
//  Registramos celda con la celda personalizada
        tablaMov.register(UINib(nibName: "CeldaPersonalizadaTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")
        
//        Llamamos a los delegados de los managers al incio estos delegados ya contienen el objeto con los datos decodificados
        managerCuenta.delegate = self
        managerSaldos.delegate = self
        managerTarjetas.delegate = self
        managerMov.delegate = self
        
//        Delegados de la tabla para que se puedan mostrar
        tablaMov.delegate = self
        tablaMov.dataSource = self
        
//        llamamos a las funciones de cada manager para que consuma las apis
        managerCuenta.crearApi()
        managerSaldos.crearApi()
        managerTarjetas.crearApi()
        managerMov.crearApi()

        
//        Sustituir variables graficas con info de la api cuentas
        labelNombreUsuario.text = arrayCuenta?[0].nombre
        labelUltimoInicio.text = arrayCuenta?[0].ultimaSesion
//        Sustituir variables graficas con info de la api saldos
        labelSaldoGeneral.text = "\(arraySaldos?[0].saldoGeneral ?? 0)"
        labelTotalIngresos.text = "\(arraySaldos?[0].ingresos ?? 0)"
//        Sustituir variables graficas con info de la api tarjetas
        labelNumTarjeta1.text = arrayTarjetas?[0].tarjeta ?? ""
        labelNombre1.text = arrayTarjetas?[0].nombre ?? ""
        labelSaldo1.text = "$ \(arrayTarjetas?[0].saldo ?? 0)"
        labelTipo1.text = arrayTarjetas?[0].tipo ?? ""
        labelEstado1.text = arrayTarjetas?[0].estado ?? ""
//        Tarjeta 2
        labelNumTarjeta2.text = arrayTarjetas?[1].tarjeta ?? ""
        labelNombre2.text = arrayTarjetas?[1].nombre ?? ""
        labelSaldo2.text = "$ \(arrayTarjetas?[1].saldo ?? 0)"
        labelTipo2.text = arrayTarjetas?[1].tipo ?? ""
        labelEstado2.text = arrayTarjetas?[1].estado ?? ""
        
//        Estado de la tarjeta 1
        if arrayTarjetas?[0].estado == "activa"{
            ivTarjeta1.image = UIImage(named: "activa")
        }else{
            ivTarjeta1.image = UIImage(named: "inactiva")
        }
//        Estado de la tarjeta 2
        if arrayTarjetas?[1].estado == "activa"{
            ivTarjeta2.image = UIImage(named: "activa")
        }else{
            ivTarjeta2.image = UIImage(named: "inactiva")
        }
        
    }
//    Boton agregar, abre la ventana de formulario
    @IBAction func btnAgregarTarjeta(_ sender: UIButton) {
        
        performSegue(withIdentifier: "segueAsociarTarjeta", sender: self)
    }
    
}

// MARK: Extension para la tabla
extension ViewController: UITableViewDataSource, UITableViewDelegate{

//    Cuantos elementos se agregaran a la tabla
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrayMov?.count ?? 0
}

//    llenamos la tablita, con la celda personalizada
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let celda = tablaMov.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! CeldaPersonalizadaTableViewCell

    celda.labelFecha.text = arrayMov?[indexPath.row].fecha
    celda.labelDescripcion.text = arrayMov?[indexPath.row].descripcion
    celda.labelTipo.text = arrayMov?[indexPath.row].tipo
    celda.labelMonto.text = "$\(arrayMov?[indexPath.row].monto ?? "")"
    
//    Si es abono verde y si no rojo
    if celda.labelTipo.text == "abono"{
        celda.labelMonto.textColor = .green
    }else{
        celda.labelMonto.textColor = .red
    }
    return celda
}
}

//Estas extensiones son para poder utilizar la funcion del protocolo y actualizar la tabla y los arrays llenos al iniciar la app
// MARK: Extension para el protocolo cuentas que actualiza el VC
extension ViewController: ManagerProtocolCuenta{
    func actualizarUI(recibeArrayCuentas: [Cuenta]) {
        arrayCuenta = recibeArrayCuentas
    }
}

// MARK: Extension para el protocolo saldos que actualiza el VC
extension ViewController: ManagerProtocolSaldos{
    func actualizarUI(recibeArraySaldos: [Saldos]) {
        arraySaldos = recibeArraySaldos
    }
}

// MARK: Extension para el protocolo Tajetas que actualiza el VC
extension ViewController: ManagerProtocolTarjetas{
    func actualizarUI(recibeArrayTarjetas: [Tarjetas]) {
        arrayTarjetas = recibeArrayTarjetas
    }
}

// MARK: Extension para el protocolo Movs que actualiza el VC
extension ViewController: ManagerProtocolMov{
    func actualizarUI(recibeArrayMov: [Movimiento]) {
        arrayMov = recibeArrayMov
        tablaMov.reloadData()
    }
}


