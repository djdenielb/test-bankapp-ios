//
//  CeldaPersonalizadaTableViewCell.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 24/05/22.
//

import UIKit

class CeldaPersonalizadaTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFecha: UILabel!
    @IBOutlet weak var labelDescripcion: UILabel!
    @IBOutlet weak var labelTipo: UILabel!
    @IBOutlet weak var labelMonto: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
