//
//  MovData.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

// Estructura para consumir api de movimientos

struct ContieneMov: Codable{
    let movimientos: [Movimiento]
}

struct Movimiento: Codable{
    let fecha: String?
    let descripcion: String?
    let monto: String?
    let tipo: String?
}
