//
//  TarjetaData.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

// Estructura para consumir api de tarjetas

struct ContieneTarjetas: Codable{
    let tarjetas: [Tarjetas]
}

struct Tarjetas: Codable{
    let tarjeta: String?
    let nombre: String?
    let saldo: Int?
    let estado: String?
    let tipo: String?
    
}
