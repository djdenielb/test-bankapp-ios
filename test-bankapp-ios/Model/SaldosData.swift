//
//  SaldosData.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

// Estructura para consumir api de saldos

struct ContieneSaldos: Codable{
    let saldos: [Saldos]
}

struct Saldos: Codable{
    let saldoGeneral: Int?
    let ingresos: Int?
}
