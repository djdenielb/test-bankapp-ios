//
//  ManagerTarjetas.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

//Protocolo para actualizar la UI
protocol ManagerProtocolTarjetas{
    func actualizarUI(recibeArrayTarjetas: [Tarjetas])
}


struct ManagerTarjetas{

    var delegate: ManagerProtocolTarjetas?

    func crearApi(){
        let urlString = "http://bankapp.endcom.mx/api/bankappTest/tarjetas"
        if let urlDesdeString = URL(string: urlString){
            if let urlData = try? Data(contentsOf: urlDesdeString){
                if let objetoParaLlenar = decodificaJson(recibeData: urlData){
                    delegate?.actualizarUI(recibeArrayTarjetas: objetoParaLlenar)
                }
            }
        }
    }

    func decodificaJson(recibeData: Data)->[Tarjetas]? {

        var arrayTarjetas: [Tarjetas] = []

        let decodificador = JSONDecoder()

        if let listaElementos = try? decodificador.decode(ContieneTarjetas.self, from: recibeData){
            arrayTarjetas = listaElementos.tarjetas

            print("Debug tarjetas \(arrayTarjetas[0].tarjeta ?? "")")
            print("Debug tarjetas \(arrayTarjetas[0].nombre ?? "")")
            print("Debug tarjetas \(arrayTarjetas[0].saldo ?? 0)")
            print("Debug tarjetas \(arrayTarjetas[0].estado ?? "")")
            print("Debug tarjetas \(arrayTarjetas[0].tipo ?? "")")

            return arrayTarjetas
        }
        return nil
    }
}
