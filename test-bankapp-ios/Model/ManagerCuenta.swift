//
//  Manager.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

//Protocolo para actualizar la UI
protocol ManagerProtocolCuenta{
    func actualizarUI(recibeArrayCuentas: [Cuenta])
}


//Manager en donde se decodificara el json
struct ManagerCuenta{

//    delegado que llevara el objeto lleno a otra pantalla
    var delegate: ManagerProtocolCuenta?

//    Funcion para crear una url de datos a partir de la url del json
    func crearApi(){
        let urlString = "http://bankapp.endcom.mx/api/bankappTest/cuenta"
        if let urlDesdeString = URL(string: urlString){
            if let urlData = try? Data(contentsOf: urlDesdeString){
                if let objetoParaLlenar = decodificaJson(recibeData: urlData){
                    delegate?.actualizarUI(recibeArrayCuentas: objetoParaLlenar)
                }
            }
        }
    }

//    Funcion que decodifica el json y lo guarda en un array de objeto
    func decodificaJson(recibeData: Data)->[Cuenta]? {

        var arrayCuenta: [Cuenta] = []

        let decodificador = JSONDecoder()

        if let listaElementos = try? decodificador.decode(ContieneCuentas.self, from: recibeData){
            arrayCuenta = listaElementos.cuenta

//            Debug para probar que esten llegando bien los datos
            print("Debug \(arrayCuenta[0].nombre ?? "Sin datos")")
            print("Debug \(arrayCuenta[0].ultimaSesion ?? "Sin datos")")

            return arrayCuenta
        }
        return nil
    }
}
