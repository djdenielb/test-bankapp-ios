//
//  ManagerSaldos.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

//Protocolo para actualizar la UI
protocol ManagerProtocolSaldos{
    func actualizarUI(recibeArraySaldos: [Saldos])
}


struct ManagerSaldos{

    var delegate: ManagerProtocolSaldos?

    func crearApi(){
        let urlString = "http://bankapp.endcom.mx/api/bankappTest/saldos"
        if let urlDesdeString = URL(string: urlString){
            if let urlData = try? Data(contentsOf: urlDesdeString){
                if let objetoParaLlenar = decodificaJson(recibeData: urlData){
                    delegate?.actualizarUI(recibeArraySaldos: objetoParaLlenar)
                }
            }
        }
    }

    func decodificaJson(recibeData: Data)->[Saldos]? {

        var arraySaldos: [Saldos] = []

        let decodificador = JSONDecoder()

        if let listaElementos = try? decodificador.decode(ContieneSaldos.self, from: recibeData){
            arraySaldos = listaElementos.saldos

            print("Debug saldo \(arraySaldos[0].ingresos ?? 0)")
            print("Debug saldo \(arraySaldos[0].saldoGeneral ?? 0)")

            return arraySaldos
        }
        return nil
    }
}
