//
//  Cuenta.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

// Estructura para consumir api de cuentas
struct ContieneCuentas: Codable{
    let cuenta: [Cuenta]
}

struct Cuenta: Codable{
    let nombre: String?
    let ultimaSesion: String?
}
