//
//  ManagerMov.swift
//  test-bankapp-ios
//
//  Created by djdenielb on 25/05/22.
//

import Foundation

//Protocolo para actualizar la UI
protocol ManagerProtocolMov{
    func actualizarUI(recibeArrayMov: [Movimiento])
}


struct ManagerMov{

    var delegate: ManagerProtocolMov?

    func crearApi(){
        let urlString = "http://bankapp.endcom.mx/api/bankappTest/movimientos"
        if let urlDesdeString = URL(string: urlString){
            if let urlData = try? Data(contentsOf: urlDesdeString){
                if let objetoParaLlenar = decodificaJson(recibeData: urlData){
                    delegate?.actualizarUI(recibeArrayMov: objetoParaLlenar)
                }
            }
        }
    }

    func decodificaJson(recibeData: Data)->[Movimiento]? {

        var arrayMov: [Movimiento] = []

        let decodificador = JSONDecoder()

        if let listaElementos = try? decodificador.decode(ContieneMov.self, from: recibeData){
            arrayMov = listaElementos.movimientos

            print("Debug movs \(arrayMov[0].descripcion ?? "")")
            print("Debug movs \(arrayMov[0].tipo ?? "")")

            return arrayMov
        }
        return nil
    }
}
