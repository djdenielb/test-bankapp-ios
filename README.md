El paso 13 es agregar un readme con informacion tecnica de la app
- Se utilizo Swift 5
- Se utilizo el storyboard
- Se trato de implementar el modelo MVC
- Se crearon archivos swift para cada api, donde se pusieron las struct y variables que tenia
- Se crearon archivos manager para decodificar las API con JsonDecoder
- Se utilizo el protocolo y delegado para enviar los objetos decodificados la pantalla del view controller y poder actualizar la vista
- Al poner activa o desactivada la tarjeta hace cambio de imagen a mas opaca la tarjeta
- Se utilizo el tableView y se crearon sus metodos obligatorios de cuantas celdas y el llenado de la celda
- Se creo una celda personaliada Xib y se agrego a la tableview 
- Se utilizo el segue para ir a la pantalla de formulario
- En la pantalla de formulario se utilizaron los texview y 2 botones
- Se creo una alerta para que todos los campos del formulario se mostraran en esa alerta con un formato tipo Json
- Se agregaron funciones para cerrar teclado al presionar fuera en cualquier lugar, al boton cancelar para que cierre la view del formulario 

